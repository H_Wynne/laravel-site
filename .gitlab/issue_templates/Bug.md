Summary:

(Provide a brief summary of the bug)

(Please avoid using words like buggy/broken/not working - it's really important to know exactly what's not working as expected)

Actual behaviour:

(What is happening that you think shouldn't happen?)

(Pictures paint a thousand words and really help us understand what's going on - include screenshots if possible)

Expected behaviour:

(What do you expect the website should do?)

Steps to reproduce:

(Provide as much information regarding what were doing when the bug occurred)

(If you were saving data to the site, please also include it here in a ```code block``` or provide a reference to it)

When did it happen?:

(This will let us see if there is any helpful information in the server logs)

\label ~type: bug